import axios from "axios";

export const API = axios.create({ baseURL: 'https://note-keepeer.herokuapp.com' })