import React, { useEffect, useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { register } from "../../redux/actions/userActions";
import Loading from "../Loading";
import MainScreen from "../MainScreen";

const Login = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [pic, setPic] = useState(
    "https://icon-library.com/images/anonymous-avatar-icon/anonymous-avatar-icon-25.jpg"
  );
  const [password, setPassword] = useState("");
  const [confirmpassword, setConfirmPassword] = useState("");
  const [picMessage, setPicMessage] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  let navigate = useNavigate();
  const dispatch = useDispatch();
  const userRegister = useSelector((state) => state.userRegister);
  const { loading, error, userInfo } = userRegister;

  useEffect(() => {
    if (userInfo) {
      navigate("/my-notes");
    }
  }, [userInfo, navigate]);

  const postDetails = (pics) => {
    if (!pics) {
      return setPicMessage("Please select image");
    }

    setPicMessage(null);
    if (
      pics.type === "image/jpeg" ||
      pics.type === "image/png" ||
      pics.type === "image/jpg"
    ) {
      const data = new FormData();
      data.append("file", pics);
      data.append("upload_preset", "note-keeper");
      data.append("cloud_name", "job-search-agency");
      fetch("https://api.cloudinary.com/v1_1/job-search-agency/image/upload", {
        method: "POST",
        body: data,
      })
        .then((response) => response.json())
        .then((data) => {
          setPic(data.url.toString());
          console.log(data.url.toString());
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      return setPicMessage("Please select image");
    }
  };

  const registerHandler = async (e) => {
    e.preventDefault();
    if (!name && !email && !password && !confirmpassword) {
      setErrorMessage(
        "Name, Email, Password or Confirm password are required!"
      );
      return;
    }

    if (password !== confirmpassword) {
      return setErrorMessage("Passwords doesnt match!");
    }

    dispatch(register(name, email, password, pic));
  };
  return (
    <MainScreen title="Register">
      {loading && <Loading />}
      <Form onSubmit={registerHandler}>
        {errorMessage && <p className="error">{errorMessage}</p>}
        {error && <p className="error">{error}</p>}
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Farid"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="name@example.com"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput4">
          <Form.Label>Password confirm</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password confirm"
            value={confirmpassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </Form.Group>

        {picMessage && <p className="error">{picMessage}</p>}
        <Form.Group controlId="formFile" className="mb-3">
          <Form.Label>Picture</Form.Label>
          <Form.Control
            type="file"
            onChange={(e) => postDetails(e.target.files[0])}
            custom
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
      <Row className="py-3">
        <Col>
          Have an account? <Link to="/login">Login here</Link>
        </Col>
      </Row>
    </MainScreen>
  );
};

export default Login;
