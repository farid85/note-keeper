import React, { useEffect, useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { login } from "../../redux/actions/userActions";
import { API } from "../../api";
import Loading from "../Loading";
import MainScreen from "../MainScreen";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);

  let navigate = useNavigate();
  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  useEffect(() => {
    if (userInfo) {
      navigate("/my-notes");
    }
  }, [navigate, userInfo]);

  const loginHandler = async (e) => {
    e.preventDefault();
    if (!email && !password) {
      setErrorMessage("Email and password are required!");
      return;
    }
    dispatch(login(email, password));
  };

  return (
    <MainScreen title="Login">
      {loading && <Loading />}
      <Form onSubmit={loginHandler}>
        <p className="error">{errorMessage && errorMessage}</p>
       { error && <p className="error">{error}</p>}
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="name@example.com"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
      <Row className="py-3">
        <Col>
          New customer <Link to="/register">Register here</Link>
        </Col>
      </Row>
    </MainScreen>
  );
};

export default Login;
