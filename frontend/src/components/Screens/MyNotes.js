import React, { useEffect } from "react";
import MainScreen from "../MainScreen";
import { Link, useNavigate } from "react-router-dom";
import ReactMarkdown from "react-markdown";
import { Accordion, Badge, Button, Card } from "react-bootstrap";
// import { notes } from "../../data";
import CustomToggle from "../helpers/CustomToggle";
import { deleteNote, fetchNotes } from "../../redux/actions/notesActions";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../Loading";
import { logout } from "../../redux/actions/userActions";

const MyNotes = ({ search }) => {
  const { notes, loading, error } = useSelector((state) => state.noteList);
  const { userInfo } = useSelector((state) => state.userLogin);
  const noteCreate = useSelector((state) => state.noteCreate);
  const { success: successCreate } = noteCreate;

  const noteUpdate = useSelector((state) => state.noteUpdate);
  const { success: successUpdate } = noteUpdate;

  const noteDelete = useSelector((state) => state.noteDelete);
  const {
    loading: loadingDelete,
    error: errorDelete,
    success: successDelete,
  } = noteDelete;

  const dispatch = useDispatch();
  let navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchNotes());
    if (!userInfo) {
      navigate("/");
    }
  }, [
    dispatch,
    navigate,
    successCreate,
    userInfo,
    successUpdate,
    successDelete,
  ]);

  const deleteHandler = (id) => {
    if (window.confirm("Are you sure?")) {
      dispatch(deleteNote(id));
    }
  };

  console.log(notes);

  // logout user if token expired
  const parseJwt = (token) => {
    try {
      return JSON.parse(atob(token.split(".")[1]));
    } catch (e) {
      return null;
    }
  };

  const user = JSON.parse(localStorage.getItem("userInfo"));

  if (user) {
    const decodedJwt = parseJwt(user.token);

    // console.log(decodedJwt)
    // console.log(decodedJwt.exp*1000)
    // console.log(Date.now())

    if (decodedJwt.exp * 1000 < Date.now()) {
      dispatch(logout());
      navigate("/login");
    }
  }
  return (
    <MainScreen title={`Welcome back ${userInfo.name}`}>
      <Link to="/create-note">
        <Button>Create new note</Button>
      </Link>
      {error && <p className="error">{error}</p>}
      {loading && <Loading />}
      {loadingDelete && <Loading />}
      {notes &&
        notes
          .filter(
            (filteredNote) =>
              filteredNote.title.toLowerCase().includes(search.toLowerCase()) &&
              filteredNote.user === userInfo._id // notes for own user this meaning each user see their own notes.
          )
          .reverse()
          .map((note) => (
            <Accordion key={note._id}>
              <Card style={{ margin: 10 }}>
                <Card.Header style={{ display: "flex" }} as="h5">
                  <span
                    style={{
                      color: "black",
                      textDecoration: "none",
                      cursor: "pointer",
                      flex: 1,
                      alignSelf: "center",
                    }}
                  >
                    <CustomToggle eventKey="0">{note.title}</CustomToggle>
                  </span>
                  <div>
                    <Button href={`/note/${note._id}`}>Edit</Button>
                    <Button
                      variant="danger"
                      className="mx-2"
                      onClick={() => deleteHandler(note._id)}
                    >
                      Delete
                    </Button>
                  </div>
                </Card.Header>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <h4>
                      <Badge bg="success">Category - {note.category}</Badge>
                    </h4>
                    <blockquote className="blockquote mb-0">
                      <ReactMarkdown>{note.content}</ReactMarkdown>
                      <footer className="blockquote-footer">
                        Created on: {note.createdAt.substring(0, 10)}
                      </footer>
                    </blockquote>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          ))}
    </MainScreen>
  );
};

export default MyNotes;
