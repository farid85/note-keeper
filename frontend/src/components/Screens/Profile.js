import React, { useEffect, useState } from "react";
import { Card, Form, Button, Row, Col, Alert } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { update } from "../../redux/actions/userActions";
import Loading from "../Loading";
import MainScreen from "../MainScreen";

const Profile = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [pic, setPic] = useState("");
  const [password, setPassword] = useState("");
  const [confirmpassword, setConfirmPassword] = useState("");
  const [picMessage, setPicMessage] = useState(null);
  const [date, setDate] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const dispatch = useDispatch();
  let navigate = useNavigate();

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const userUpdate = useSelector((state) => state.userUpdate);
  const { loading, error, success } = userUpdate;

  useEffect(() => {
    if (!userInfo) {
      navigate("/login");
    } else {
      setName(userInfo.name);
      setEmail(userInfo.email);
      setPic(userInfo.pic);
    }
  }, [navigate, userInfo]);

  const postDetails = (pics) => {
    if (!pics) {
      return setPicMessage("Please select image");
    }

    setPicMessage(null);
    if (
      pics.type === "image/jpeg" ||
      pics.type === "image/png" ||
      pics.type === "image/jpg"
    ) {
      const data = new FormData();
      data.append("file", pics);
      data.append("upload_preset", "note-keeper");
      data.append("cloud_name", "job-search-agency");
      fetch("https://api.cloudinary.com/v1_1/job-search-agency/image/upload", {
        method: "POST",
        body: data,
      })
        .then((response) => response.json())
        .then((data) => {
          setPic(data.url.toString());
          console.log(data.url.toString());
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      return setPicMessage("Please select image");
    }
  };

  const updateProfileHandler = (e) => {
    e.preventDefault();

    console.log(name, email, password, pic);
    console.log(password.length)

    if (password === confirmpassword) {
      dispatch(update({ name, email, password, pic }));
    }
  };
  return (
    <MainScreen title="Update Profile">
      {loading && <Loading />}
      <Row>
        <Col>
          <Card>
            {errorMessage && <p className="error">{errorMessage}</p>}
            {error && <p className="error">{error}</p>}
            {success && <Alert variant="success">Updated Successfully</Alert>}
            <Card.Header>Update profile {name}</Card.Header>
            <Card.Body>
              <Form onSubmit={updateProfileHandler}>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Farid"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput2"
                >
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="name@example.com"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput3"
                >
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput4"
                >
                  <Form.Label>Password confirm</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password confirm"
                    value={confirmpassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </Form.Group>

                {picMessage && <p className="error">{picMessage}</p>}
                <Form.Group controlId="formFile" className="mb-3">
                  <Form.Label>Picture</Form.Label>
                  <Form.Control
                    type="file"
                    onChange={(e) => postDetails(e.target.files[0])}
                    custom
                  />
                </Form.Group>

                <Button variant="primary" type="submit">
                  Update
                </Button>
              </Form>
            </Card.Body>
            <Card.Footer>Created on - {date.substring(0, 10)}</Card.Footer>
          </Card>
        </Col>
        <Col
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img src={pic} alt={name} className="profilePic" />
        </Col>
      </Row>
    </MainScreen>
  );
};

export default Profile;
