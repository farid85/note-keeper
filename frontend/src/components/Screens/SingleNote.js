import React, { useEffect, useState } from "react";
import { Col, Form, Row, Button, Card } from "react-bootstrap";
import { Link, useNavigate, useParams } from "react-router-dom";
import MainScreen from "../MainScreen";
import ReactMarkdown from "react-markdown";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../Loading";
import { createNote, deleteNote, updateNote } from "../../redux/actions/notesActions";
import { API } from "../../api";

const SingleNote = ({ match }) => {
  const [title, setTitle] = useState("");
  const [content, setContnet] = useState("");
  const [category, setCategory] = useState("");
  const [date, setDate] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const { loading, error } = useSelector((state) => state.noteUpdate);
  const { userInfo } = useSelector((state) => state.userLogin);

  const dispatch = useDispatch();
  let navigate = useNavigate();
  let {id} = useParams();

  useEffect(() => {
    const fetching = async () => {
        const config = {
            headers: {
                Authorization: `Bearer ${userInfo.token}`,
            }
        }
      const { data } = await API.get(`/api/notes/${id}`, config);
      setTitle(data.title);
      setContnet(data.content);
      setCategory(data.category);
      setDate(data.updatedAt);
    };

    fetching();
  }, [id, date]);

  const noteDelete = useSelector((state) => state.noteDelete);
  const { loading: loadingDelete, error: errorDelete } = noteDelete;

  const deleteHandler = (id) => {
    if (window.confirm("Are you sure?")) {
      dispatch(deleteNote(id));
    }
    navigate("/my-notes");
  };

  const updateNoteHandler = (e) => {
    e.preventDefault();
    dispatch(updateNote(id, title, content, category));
    resetHandler()
    navigate("/my-notes");
  };

  const resetHandler = () => {
    setTitle("");
    setContnet("");
    setCategory("");
  };
  return (
    <MainScreen title="Update Note">
      {loading && <Loading />}
      <Card>
        <Card.Header>Update this note</Card.Header>
        <Card.Body>
          <Form onSubmit={updateNoteHandler}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
              <Form.Label>Content</Form.Label>
              <Form.Control
                as="textarea"
                rows={4}
                placeholder="Content"
                value={content}
                onChange={(e) => setContnet(e.target.value)}
              />
            </Form.Group>
            {content && (
              <Card>
                <Card.Header>Preview</Card.Header>
                <Card.Body>
                  <ReactMarkdown>{content}</ReactMarkdown>
                </Card.Body>
              </Card>
            )}
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              />
            </Form.Group>

            <Row className="py-3">
              <Col>
                <Button variant="primary" type="submit">
                  Update
                </Button>
                <Button
                  onClick={()=>deleteHandler(id)}
                  className="mx-2"
                  variant="danger"
                >
                  Delete
                </Button>
                <Link to="/my-notes">
                  <Button variant="secondary">Cancel</Button>
                </Link>
              </Col>
            </Row>
          </Form>
        </Card.Body>
        <Card.Footer>
          Updating on - {date.substring(0, 10)}
        </Card.Footer>
      </Card>
    </MainScreen>
  );
};

export default SingleNote;
