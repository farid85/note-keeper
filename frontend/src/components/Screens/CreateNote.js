import React, { useState } from "react";
import { Col, Form, Row, Button, Card } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import MainScreen from "../MainScreen";
import ReactMarkdown from "react-markdown";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../Loading";
import { createNote } from "../../redux/actions/notesActions";

const CreateNote = () => {
  const [title, setTitle] = useState("");
  const [content, setContnet] = useState("");
  const [category, setCategory] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const {note, loading, error} = useSelector(state => state.noteCreate)

  const dispatch = useDispatch()
  let navigate = useNavigate();

  const createNoteHandler = (e) => {
    e.preventDefault();

    if (!title && !content && !category) {
        return setErrorMessage("Please fill fields!")
    }

    dispatch(createNote({title, content, category}));
    resetHandler();
    navigate("/my-notes")

  };

  const resetHandler = () => {
      setTitle('');
      setContnet('');
      setCategory('');
  }
  return (
    <MainScreen title="Create Note">
      {loading && <Loading />}
      <Card>
        <Card.Header>Create new a note</Card.Header>
        <Card.Body>
          <Form onSubmit={createNoteHandler}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
              <Form.Label>Content</Form.Label>
              <Form.Control
                as="textarea"
                rows={4}
                placeholder="Content"
                value={content}
                onChange={(e) => setContnet(e.target.value)}
              />
            </Form.Group>
            {content && (
              <Card>
                <Card.Header>Preview</Card.Header>
                <Card.Body>
                  <ReactMarkdown>{content}</ReactMarkdown>
                </Card.Body>
              </Card>
            )}
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              />
            </Form.Group>

            <Row className="py-3">
              <Col>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
                <Button onClick={resetHandler} className="mx-2" variant="danger">
                  Reset fields
                </Button>
                <Link to="/my-notes">
                  <Button variant="secondary">Cancel</Button>
                </Link>
              </Col>
            </Row>
          </Form>
        </Card.Body>
        <Card.Footer>
            Creating on - {new Date().toLocaleDateString()}
        </Card.Footer>
      </Card>
    </MainScreen>
  );
};

export default CreateNote;
