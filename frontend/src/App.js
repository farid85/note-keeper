import "./App.css";
import {
  BrowserRouter,
  Routes,
  Route,
  Outlet,
  Navigate,
} from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import LandingPage from "./components/Screens/LandingPage";
import MyNotes from "./components/Screens/MyNotes";
import Login from "./components/Screens/Login";
import Register from "./components/Screens/Register";
import CreateNote from "./components/Screens/CreateNote";
import SingleNote from "./components/Screens/SingleNote";
import { useState } from "react";
import { useSelector } from "react-redux";
import Profile from "./components/Screens/Profile";

function App() {
  const [search, setSearch] = useState("");

  function PrivateOutlet() {
    const { userInfo } = useSelector((state) => state.userLogin);
    return userInfo ? <Outlet /> : <Navigate to="/" />;
  }
  return (
    <BrowserRouter>
      <Header setSearch={setSearch} />
      <main>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/my-notes" element={<MyNotes search={search} />} />
          <Route path="/create-note" element={<PrivateOutlet />}>
            <Route path="" element={<CreateNote />} />
          </Route>
          <Route path="/profile" element={<PrivateOutlet />}>
            <Route path="" element={<Profile />} />
          </Route>
          <Route path="/note/:id" element={<SingleNote />} />
        </Routes>
      </main>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
