const express = require("express");
const {
  registerUser,
  loginUser,
  updateProfile,
//   getUsers,
} = require("../controllers/userControllers");
const { protect } = require("../middlewares/authMiddleware");

const router = express.Router();


router.post("/login", loginUser);
router.post("/register", registerUser);
router.put("/profile", protect, updateProfile);

module.exports = router;
