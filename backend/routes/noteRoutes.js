const express = require("express");
const {
  getNotes,
  createNote,
  getNote,
  updateNote,
  deleteNote,
} = require("../controllers/noteControllers");
const { protect } = require("../middlewares/authMiddleware");

const router = express.Router();


router.get("/", protect, getNotes);
router.get("/:id", protect, getNote);
router.put("/:id", protect, updateNote);
router.delete("/:id", protect, deleteNote);
router.post("/create", protect, createNote);

module.exports = router;
