const express = require("express");
const app = express();
const conncectDB = require("./config/db");
const cors = require("cors");
const dotenv = require("dotenv");
const notes = require("./config/data");
const path = require("path");
dotenv.config();

const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

// Import routes
const userRoutes = require("./routes/userRoutes");
const noteRoutes = require("./routes/noteRoutes");


//Middleware routes
app.use("/api/user", userRoutes);
app.use("/api/notes", noteRoutes);

// --------------------------deployment------------------------------
const __dirname1 = path.resolve();

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname1, "/frontend/build")));

  app.get("*", (req, res) =>
    res.sendFile(path.resolve(__dirname1, "frontend", "build", "index.html"))
  );
} else {
  app.get("/", (req, res) => {
    res.send("API is running..");
  });
}
// --------------------------deployment------------------------------

//routes
// app.get("/", (req, res) => {
//   res.send("Hello World! Nodejs is perfect");
// });
// app.get("/api/notes", (req, res) => {
//   res.json(notes);
// });
// app.get("/api/notes/:id", (req, res) => {
//   const note = notes.find((n) => n._id === req.params.id);
//   res.json(note);
// });



// Error Handling middlewares
const { notFound, errorHandler } = require("./middlewares/errorMiddleware");
app.use(notFound);
app.use(errorHandler);

// connect to DB
conncectDB();

//start on server
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
